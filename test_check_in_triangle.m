clc; clear all; close all

% test check_in_triangle
P1 = [1,0];
P2 = [-1,0];
P3 = [0,2];

P = [0.5,1+1e-22]; % choose a point on the boundary :)

result = check_in_triangle(P,P1,P2,P3)

P_all = [P1; P2; P3; P1];
figure; hold on
plot(P_all(:,1), P_all(:,2), 'o-k','linewidth',1,'markersize',10)
plot(P(1),P(2),'xb','linewidth',2)
if result
    r = 'true';
else
    r = 'false';
end
title(sprintf('%s',r))

%{
for point P lying on a mesh boundry of >1 triangles,
what happens w.r.t. precision?

it should only lie on one of the triangles

how to resolve?

TRY:
check if in boundary area (thickness) and pick a random side

for P on boundary between mesh and non-mesh, assume P is in mesh
%}