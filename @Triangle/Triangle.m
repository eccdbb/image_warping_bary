classdef Triangle < handle
    properties
        vertex_1 % Node instance
        vertex_2
        vertex_3
        vertex_1_value % RGB value
        vertex_2_value
        vertex_3_value
    end
    properties (Constant)
        
    end
    methods
        function obj = Triangle(vertex_1,vertex_2,vertex_3,value_1,value_2,value_3)
            obj.vertex_1 = vertex_1;
            obj.vertex_2 = vertex_2;
            obj.vertex_3 = vertex_3;
            obj.vertex_1_value = value_1;
            obj.vertex_2_value = value_2;
            obj.vertex_3_value = value_3;
        end
    end
    methods (Static)
        
    end
end