clc; clear all; close all;


%% setup

% filename = 'paimon (genshin impact) drawn by rosuuri - da15acf017886d726d3ec9f2dcca5977.jpg';
% filename = 'Capture.png';
filename = 'Capture2.png';
% filename = 'Capture3.png';

%% form triangle grid for barycentric interpolation

x = Barycentric(filename);
% x = x.define_graph()
% h = x.plot_graph();
% set(h,'position',[50 50 1000 1000])


% x = x.define_graph_2()
% h2 = x.plot_graph_2();
% set(h2,'position',[150 50 1000 1000])

% x = x.define_graph_3()
h3 = x.plot_graph_4(false);
set(h3,'position',[150 50 1000 1000])

%% apply deformation
% jiggle each node (add noise)
L = length(x.node_list);
for i = 1:L
    x.node_list{i}.ij_deformed = x.node_list{i}.ij + (rand(1,2)-0.5)/2;
end
h4 = x.plot_graph_4(true);
set(h4,'position',[450 50 1000 1000])
axis equal
axis ij

% do barycentric interpolation
new_img = x.interpolate();
figure;
set(gcf,'position',[450 950 1000 1000])
subplot(121)
imshow(x.img)
subplot(122)
imshow(new_img)

%% apply rotation deformation
x = x.deform_1();
h5 = x.plot_graph_4(true);
set(h5,'position',[550 50 1000 1000])
set(gca,'xlimmode','auto')
set(gca,'ylimmode','auto')
axis equal
axis ij
x.show_sampling_grid(h5)

% do barycentric interpolation
new_img = x.interpolate();
figure;
set(gcf,'position',[550 950 1000 1000])
subplot(121)
imshow(x.img)
subplot(122)
imshow(new_img)

%% apply shear deformation
x = x.deform_2();
h6 = x.plot_graph_4(true);
set(h6,'position',[650 50 1000 1000])
set(gca,'xlimmode','auto')
set(gca,'ylimmode','auto')
axis equal
axis ij
x.show_sampling_grid(h6);

% do barycentric interpolation
new_img = x.interpolate();
figure;
set(gcf,'position',[650 950 1000 1000])
subplot(121)
imshow(x.img)
subplot(122)
imshow(new_img)

%% apply scale deformation
x = x.deform_3();
h7 = x.plot_graph_4(true);
set(h7,'position',[750 50 1000 1000])
set(gca,'xlimmode','auto')
set(gca,'ylimmode','auto')
axis equal
axis ij
x.show_sampling_grid(h7);

% do barycentric interpolation
new_img = x.interpolate();
figure;
set(gcf,'position',[750 950 1000 1000])
subplot(121)
imshow(x.img)
subplot(122)
imshow(new_img)

%% apply wavy deformation
x = x.deform_4();
h8 = x.plot_graph_4(true);
set(h8,'position',[850 50 1000 1000])
set(gca,'xlimmode','auto')
set(gca,'ylimmode','auto')
axis equal
axis ij
x.show_sampling_grid(h8);

% do barycentric interpolation
new_img = x.interpolate();
figure;
set(gcf,'position',[850 950 1000 1000])
subplot(121)
imshow(x.img)
subplot(122)
imshow(new_img)

%% done
disp('reminder: aliasing can occur')
disp('-- done')

% DONE: assign pixel RGB values to each vertex
% DONE: deform the entire grid
% DONE: then do barycentric interpolation
% make a Triangle class with properties: vertex_1, vertex_2, vertex_3 = Node instance_1/2/3 // DONE
% use counter when creating triangles; save list of all triangles // DONE
% find which triangle the current point is in // DONE

% TODO: how does anti-aliasing work?  should I upsample->deform and interpolate->downsample?
% TODO: quad-tree to speed up point in triangle search
%   - initialize N-depth quad-tree using cells
%   - when inserting points: recursively: get_quadrant(triangle), return quadrant address, insert data to leaf
%     - when querying point: traverse  depth and check cond (for point) until land in filled leaf
%     - if land in empty leaf, point is not in any triangle
%     - else (land in non-empty leaf), take all triangles in the leaf and check_in_triangle()
% 
% get_quadrant(triangle): let cond="all vertices are in quadrant box"; traverse depth and check cond for each quadrant; stop traversing depth if cond fails for all quadrants


% hint:
% barycentric interpolation implemented here uses the forwards mapping function [u,v]=f(x,y)
% where [x,y] is defined on an integer grid
% 
% to use bilinear/bicubic interpolation, use backwards mapping function [x,y] = f_inverse(u,v)
% assuming f() is invertible (ex. affine transformations)
% then, if [u,v] is defined on an integer grid, use bilinear/bicubic interpolation to calculate value at [x,y]
% 
% if f() is not invertible, then cannot use the backwards method