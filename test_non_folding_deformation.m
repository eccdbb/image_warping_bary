clc; clear all; close all;

% deformation without folding; how?

% no folding -> monotonic increasing function

x = 1:100;

dx = 100/2/pi * sin(2*pi/100*x); % see note on slope

figure; hold on
set(gcf,'position',[450 50 1000 1000])
plot(x,x,'k-')
plot(x,x+dx,'b--') % small slope -> pinch; large slope -> stretch

%{
note:
to prevent folding, slope of dx at any point should be > -1
%}