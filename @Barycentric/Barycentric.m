classdef Barycentric
    properties
        img
        r % image # rows
        c % image # columns
        ch % image # channels
        target_img % image with the same sampling points as img; color values are calculated from deformed image
        %A % adjacency matrix
        node_list % cell vector; each cell contains a struct with properties: i, j, neighbors, vertices; ex. node_list{#}.i/j/neighbors
        triangle_list % cell vector; list of Triangle instances
        
        % node_list is a collection of all nodes (pixel centers) for the image
    end
    properties (Constant)
        
    end
    methods
        function obj = Barycentric(filename)
            obj.img = imread(filename);
            obj.img = double(obj.img)/255; % convert to double [0,1]
            [r,c,ch] = size(obj.img);
            obj.r = r;
            obj.c = c;
            obj.ch = ch;
            obj.node_list = {};
            obj.triangle_list = {};
            
            % set graph
            obj = obj.define_graph_4();
        end
        function obj = define_graph(obj)
            %{
            o-o-o-o
            |\|\|\|
            o-o-o-o
            |\|\|\|
            o-o-o-o
            %}
            
            r = obj.r;
            c = obj.c;
            
            % graph: # nodes = # pixels = r*c
            % node order: column minor; first node (1,1) corresponds to img(1,1,:)
            %A = zeros(r*c,r*c); % graph adjacency matrix // don't use adj matrix; use list instead
            
            % initialize node list; column minor
            obj.node_list = cell(r*c,1);
            %s = struct();
            %s.neighbors = {}; % cell list; each neighbor is a [i,j]; to append a neighbor: s.neighbors{end+1} = [123,456]
            %s.i = NaN;
            %s.j = NaN;
            for i = 1:r
                for j = 1:c
                    obj.node_list{(i-1)*c+j}.neighbors = {}; % no neighbors
                    obj.node_list{(i-1)*c+j}.i = i; % also, store this node's row and column index
                    obj.node_list{(i-1)*c+j}.j = j;
                end
            end % done initializing
            disp('done intializing empty graph')
            
            % to append a neighbor to node: obj.node_list{linear node index}.neighbors{end+1} = [123,456]
            
            % set graph nodes
            for i = 1:r
                for j = 1:c
                    %disp([i,j])
                    
                    % ========= action logic ============
                    % if is top left corner: connect to right, bottom, bottom-right
                    % elseif is top right corner: connect to left, bottom
                    % elseif is bottom left corner: connect to top, right
                    % elseif is bottom right corner: connect to left, top, top-left
                    % elseif is left: connect to top, bottom, right, bottom-right
                    % elseif is right: connect to top, bottom, left, top-left
                    % elseif is top: connect to left, right, bottom, bottom-right
                    % elseif is bottom: connect to left, right, top, top-left
                    % else (not a border node): connect to left, right, top, bottom, top-left, bottom-right
                    
                    % obj = connect_to(obj,i,j,r,c, '' );
                    if Barycentric.check_top_left_corner(i,j,r,c)
                        obj = connect_to(obj,i,j,r,c, 'right' );
                        obj = connect_to(obj,i,j,r,c, 'bottom' );
                        obj = connect_to(obj,i,j,r,c, 'bottom-right' );
                    elseif Barycentric.check_top_right_corner(i,j,r,c)
                        obj = connect_to(obj,i,j,r,c, 'left' );
                        obj = connect_to(obj,i,j,r,c, 'bottom' );
                    elseif Barycentric.check_bottom_left_corner(i,j,r,c)
                        obj = connect_to(obj,i,j,r,c, 'top' );
                        obj = connect_to(obj,i,j,r,c, 'right' );
                    elseif Barycentric.check_bottom_right_corner(i,j,r,c)
                        obj = connect_to(obj,i,j,r,c, 'left' );
                        obj = connect_to(obj,i,j,r,c, 'top' );
                        obj = connect_to(obj,i,j,r,c, 'top-left' );
                    elseif Barycentric.check_left(i,j,r,c)
                        obj = connect_to(obj,i,j,r,c, 'top' );
                        obj = connect_to(obj,i,j,r,c, 'bottom' );
                        obj = connect_to(obj,i,j,r,c, 'right' );
                        obj = connect_to(obj,i,j,r,c, 'bottom-right' );
                    elseif Barycentric.check_right(i,j,r,c)
                        obj = connect_to(obj,i,j,r,c, 'top' );
                        obj = connect_to(obj,i,j,r,c, 'bottom' );
                        obj = connect_to(obj,i,j,r,c, 'left' );
                        obj = connect_to(obj,i,j,r,c, 'top-left' );
                    elseif Barycentric.check_top(i,j,r,c)
                        obj = connect_to(obj,i,j,r,c, 'left' );
                        obj = connect_to(obj,i,j,r,c, 'right' );
                        obj = connect_to(obj,i,j,r,c, 'bottom' );
                        obj = connect_to(obj,i,j,r,c, 'bottom-right' );
                    elseif Barycentric.check_bottom(i,j,r,c)
                        obj = connect_to(obj,i,j,r,c, 'left' );
                        obj = connect_to(obj,i,j,r,c, 'right' );
                        obj = connect_to(obj,i,j,r,c, 'top' );
                        obj = connect_to(obj,i,j,r,c, 'top-left' );
                    else
                        obj = connect_to(obj,i,j,r,c, 'left' );
                        obj = connect_to(obj,i,j,r,c, 'right' );
                        obj = connect_to(obj,i,j,r,c, 'top' );
                        obj = connect_to(obj,i,j,r,c, 'bottom' );
                        obj = connect_to(obj,i,j,r,c, 'top-left' );
                        obj = connect_to(obj,i,j,r,c, 'bottom-right' );
                    end
                end
            end
            disp('done constructing graph')
        end
        
        function obj = define_graph_2(obj)
            %{
            o-o-o-o  <- i=1
            |\|\|\|
            o-o-o-o
            |\|\|\|
            o-o-o-o
            
            ^
            |
            j=1
            %}
            r = obj.r;
            c = obj.c;
            
            % graph: # nodes = # pixels = r*c
            % node order: column minor; first node (1,1) corresponds to img(1,1,:)
            
            % initialize node list; column minor
            obj.node_list = cell(r*c,1);
            for i = 1:r
                for j = 1:c
                    obj.node_list{(i-1)*c+j}.vertices = {}; % no neighbors
                    obj.node_list{(i-1)*c+j}.i = i; % also, store this node's row and column index
                    obj.node_list{(i-1)*c+j}.j = j;
                end
            end % done initializing
            disp('done intializing empty graph')
            
            % to append a vertex to node: obj.node_list{linear node index}.vertices{end+1} = [tri1_i,tri1_j; tri2_i,tri_2_j; tri3_i,tri3_j] % matrix of triangle vertices
            
            % set graph nodes
            for i = 1:(r-1)
                for j = 1:(c-1)
                    % ========= action logic ============
                    % for i = 1 : (r-1), j = 1 : (c-1),
                    % top triangle virtices = (i,j) , (i,j+1) , (i+1,j+1)
                    % bottom triangle vertices = (i,j) , (i+1,j) , (i+1,j+1)
                    obj.node_list{(i-1)*c+j}.vertices{end+1} = { [i,j; i,j+1; i+1,j+1] , [reshape(obj.img(i,j,:),[],1), reshape(obj.img(i,j+1,:),[],1), reshape(obj.img(i+1,j+1,:),[],1)] }; % top triangle {vertices, RGB values}
                    obj.node_list{(i-1)*c+j}.vertices{end+1} = { [i,j; i+1,j; i+1,j+1] , [reshape(obj.img(i,j,:),[],1), reshape(obj.img(i+1,j,:),[],1), reshape(obj.img(i+1,j+1,:),[],1)] }; % bottom triangle {vertices, RGB values}
                    
                    % obj.node_list{#}.vertices{##}{2} = [R1 R2 R3; G1 G2 G3; B1 B2 B3]; each column is RGB values for a vertex
                end
            end
            disp('done constructing graph')
        end
        
        function obj = define_graph_3(obj)
            %{
            o-o-o-o  <- i=1
            |\|\|\|
            o-o-o-o
            |\|\|\|
            o-o-o-o
            
            ^
            |
            j=1
            %}
            r = obj.r;
            c = obj.c;
            
            % graph: # nodes = # pixels = r*c
            % node order: column minor; first node (1,1) corresponds to img(1,1,:)
            
            % initialize node list; column minor
            obj.node_list = cell(r*c,1);
            for i = 1:r
                for j = 1:c
                    id = (i-1)*c+j;
                    obj.node_list{id} = Node(id);
                    obj.node_list{id}.RGB = reshape(obj.img(i,j,:),[],1);
                    obj.node_list{id}.ij = [i,j];
                    obj.node_list{id}.ij_deformed = [i,j];
                end
            end
            
            % set graph nodes
            for i = 1:(r-1)
                for j = 1:(c-1)
                    
                    ii = i; jj = j; top1 = obj.node_list{(ii-1)*c+jj};
                    ii = i; jj = j+1; top2 = obj.node_list{(ii-1)*c+jj};
                    ii = i+1; jj = j+1; top3 = obj.node_list{(ii-1)*c+jj};
                    ii = i; jj = j; bot1 = obj.node_list{(ii-1)*c+jj};
                    ii = i+1; jj = j; bot2 = obj.node_list{(ii-1)*c+jj};
                    ii = i+1; jj = j+1; bot3 = obj.node_list{(ii-1)*c+jj};
                    obj.node_list{(i-1)*c+j}.triangles{1} = {top1,top2,top3}; % top triangle
                    obj.node_list{(i-1)*c+j}.triangles{2} = {bot1,bot2,bot3}; % bottom triangle
                end
            end
            disp('done constructing graph')
        end
        
        function obj = define_graph_4(obj)
            % same as define_graph_3, but using Triangle class
            %{
            o-o-o-o  <- i=1
            |\|\|\|
            o-o-o-o
            |\|\|\|
            o-o-o-o
            
            ^
            |
            j=1
            %}
            r = obj.r;
            c = obj.c;
            
            % graph: # nodes = # pixels = r*c
            % node order: column minor; first node (1,1) corresponds to img(1,1,:)
            
            % initialize node list; column minor
            obj.node_list = cell(r*c,1);
            for i = 1:r
                for j = 1:c
                    id = (i-1)*c+j;
                    obj.node_list{id} = Node(id);
                    obj.node_list{id}.RGB = reshape(obj.img(i,j,:),[],1);
                    obj.node_list{id}.ij = [i,j];
                    obj.node_list{id}.ij_deformed = [i,j];
                end
            end
            
            % initialize triangle list and counter
            obj.triangle_list = {};
            count_triangles = 0;
            
            % set graph nodes
            for i = 1:(r-1)
                for j = 1:(c-1)
                    
                    ii = i; jj = j;     top1 = obj.node_list{(ii-1)*c+jj}; top1_value = obj.img(ii,jj,:);
                    ii = i; jj = j+1;   top2 = obj.node_list{(ii-1)*c+jj}; top2_value = obj.img(ii,jj,:);
                    ii = i+1; jj = j+1; top3 = obj.node_list{(ii-1)*c+jj}; top3_value = obj.img(ii,jj,:);
                    ii = i; jj = j;     bot1 = obj.node_list{(ii-1)*c+jj}; bot1_value = obj.img(ii,jj,:);
                    ii = i+1; jj = j;   bot2 = obj.node_list{(ii-1)*c+jj}; bot2_value = obj.img(ii,jj,:);
                    ii = i+1; jj = j+1; bot3 = obj.node_list{(ii-1)*c+jj}; bot3_value = obj.img(ii,jj,:);
                    
                    obj.node_list{(i-1)*c+j}.triangles{1} = Triangle(top1,top2,top3,top1_value,top2_value,top3_value); %{top1,top2,top3}; % top triangle
                    count_triangles = count_triangles + 1;
                    obj.triangle_list{count_triangles} = obj.node_list{(i-1)*c+j}.triangles{1};
                    
                    obj.node_list{(i-1)*c+j}.triangles{2} = Triangle(bot1,bot2,bot3,bot1_value,bot2_value,bot3_value); %{bot1,bot2,bot3}; % bottom triangle
                    count_triangles = count_triangles + 1;
                    obj.triangle_list{count_triangles} = obj.node_list{(i-1)*c+j}.triangles{2};
                end
            end
            disp('done constructing graph')
        end
        
        function obj = connect_to(obj,i,j,r,c,direction_string)
            % current row index: i
            % corrent column index: j
            
            % to append a neighbor to node: obj.node_list{linear node index}.neighbors{end+1} = [123,456]
            
            % ======= action list ==========
            % connect to the left
            % connect to the right
            % connect to the top
            % connect to the bottom
            % connect to the top-left
            % connect to the bottom-right
            
            if strcmp(direction_string,'left')
                obj.node_list{(i-1)*c+j}.neighbors{end+1} = [i,j-1]; % [row,column]
            elseif strcmp(direction_string,'right')
                obj.node_list{(i-1)*c+j}.neighbors{end+1} = [i,j+1];
            elseif strcmp(direction_string,'top')
                obj.node_list{(i-1)*c+j}.neighbors{end+1} = [i-1,j];
            elseif strcmp(direction_string,'bottom')
                obj.node_list{(i-1)*c+j}.neighbors{end+1} = [i+1,j];
            elseif strcmp(direction_string,'top-left')
                obj.node_list{(i-1)*c+j}.neighbors{end+1} = [i-1,j-1];
            elseif strcmp(direction_string,'bottom-right')
                obj.node_list{(i-1)*c+j}.neighbors{end+1} = [i+1,j+1];
            end
        end
        
        function h = plot_graph(obj)
            % for each node, plot line from self to neighbors
            % return figure handle
            %
            % for use with define_graph()
            
            h = figure; hold on;
            xlim([0,obj.c+1])
            ylim([0,obj.r+1])
            L = length(obj.node_list);
            for i = 1:L % for all nodes
                neighbors = obj.node_list{i}.neighbors; % cell vector with contents [row,column] of neighbor
                L2 = length(neighbors);
                z = i/L+rem(i,obj.r)*sqrt(L);
                for ii = 1:L2 % for all neighbors
                    neighbor_i = neighbors{ii}(1);
                    neighbor_j = neighbors{ii}(2);
                    curr_i = obj.node_list{i}.i;
                    curr_j = obj.node_list{i}.j;
                    
                    plot3([curr_j,neighbor_j],[curr_i,neighbor_i],[z,z],'k')
                    %disp([curr_j,neighbor_j,curr_i,neighbor_i])
                    %pause(1)
                end
                %pause(.3)
            end
        end
        
        function h = plot_graph_2(obj)
            % for each node, plot triangles
            % return figure handle
            %
            % for use with define_graph_2()
            
            h = figure; hold on;
            xlim([0,obj.c+1])
            ylim([0,obj.r+1])
            L = length(obj.node_list);
            for i = 1:L % for all nodes
                triangles = obj.node_list{i}.vertices;
                L2 = length(triangles);
                for ii = 1:L2 % for all triangles
                    curr_triangle = triangles{ii}{1}; % get vertex coordinates
                    % reminder: triangles{ii}{2}: get vertex RGB values
                    curr_triangle = [curr_triangle; curr_triangle(1,:)]; % append first point so can draw closed triangle
                    plot(curr_triangle(:,2),curr_triangle(:,1),'-k')
                end
            end
        end
        
        function h = plot_graph_3(obj,plot_deformed)
            % for each node, plot triangles
            % return figure handle
            %
            % for use with define_graph_3()
            %
            % plot_deformed = false: plot with ij
            % plot_deformed = true: plot with ij_deformed
            
            h = figure; hold on;
            xlim([0,obj.c+1])
            ylim([0,obj.r+1])
            L = length(obj.node_list);
            for i = 1:L % for all nodes
                triangles = obj.node_list{i}.triangles;
                L2 = length(triangles);
                for ii = 1:L2 % for all triangles, if exist
                    curr_triangle = triangles{ii};
                    vertex1 = curr_triangle{1}; if plot_deformed, i1 = vertex1.ij_deformed(1); j1 = vertex1.ij_deformed(2); else, i1 = vertex1.ij(1); j1 = vertex1.ij(2); end
                    vertex2 = curr_triangle{2}; if plot_deformed, i2 = vertex2.ij_deformed(1); j2 = vertex2.ij_deformed(2); else, i2 = vertex2.ij(1); j2 = vertex2.ij(2); end
                    vertex3 = curr_triangle{3}; if plot_deformed, i3 = vertex3.ij_deformed(1); j3 = vertex3.ij_deformed(2); else, i3 = vertex3.ij(1); j3 = vertex3.ij(2); end
                    plot([j1,j2,j3,j1],[i1,i2,i3,i1],'-k')
                    %pause(0.2)
                end
            end
        end
        
        function h = plot_graph_4(obj,plot_deformed)
            % for each node, plot triangles
            % return figure handle
            %
            % for use with define_graph_4()
            %
            % plot_deformed = false: plot with ij
            % plot_deformed = true: plot with ij_deformed
            
            h = figure; hold on;
            xlim([0,obj.c+1])
            ylim([0,obj.r+1])
            L = length(obj.node_list);
            for i = 1:L % for all nodes
                triangles = obj.node_list{i}.triangles;
                L2 = length(triangles);
                for ii = 1:L2 % for all triangles, if exist
                    curr_triangle = triangles{ii};
                    vertex1 = curr_triangle.vertex_1; if plot_deformed, i1 = vertex1.ij_deformed(1); j1 = vertex1.ij_deformed(2); else, i1 = vertex1.ij(1); j1 = vertex1.ij(2); end
                    vertex2 = curr_triangle.vertex_2; if plot_deformed, i2 = vertex2.ij_deformed(1); j2 = vertex2.ij_deformed(2); else, i2 = vertex2.ij(1); j2 = vertex2.ij(2); end
                    vertex3 = curr_triangle.vertex_3; if plot_deformed, i3 = vertex3.ij_deformed(1); j3 = vertex3.ij_deformed(2); else, i3 = vertex3.ij(1); j3 = vertex3.ij(2); end
                    plot([j1,j2,j3,j1],[i1,i2,i3,i1],'-k')
                    %pause(0.2)
                end
            end
        end
        
        % note: the deform functions below may return row or column length=2 vectors...should fix // TODO
        function obj = deform_identity(obj)
            % apply deformation from ij and set ij_deformed
            % identity mapping
            
            L = length(obj.node_list);
            for i = 1:L % for all nodes
                % get current node coordinates
                curr_ij = obj.node_list{i}.ij;
                
                % apply deformation mapping
                new_ij = curr_ij;
                
                % set ij_deformed for current node
                obj.node_list{i}.ij_deformed = new_ij;
            end
        end
        
        function obj = deform_1(obj)
            % apply deformation from ij and set ij_deformed
            % rotation
            
            L = length(obj.node_list);
            for i = 1:L % for all nodes
                % get current node coordinates
                curr_ij = obj.node_list{i}.ij;
                
                % apply deformation mapping
                f_rotate = @(ij,theta) [cos(theta), -sin(theta); sin(theta), cos(theta)]*[ij(1); ij(2)];
                f_rotate_about = @(ij,theta,ij_center) f_rotate(ij-ij_center,theta) + ij_center;
                
                ij_center = [obj.r/2, obj.c/2];
                theta = pi/8;
                xy = [curr_ij(2),curr_ij(1)];
                new_xy = f_rotate_about(xy,theta,ij_center);
                new_ij = [new_xy(2),new_xy(1)];
                
                % set ij_deformed for current node
                obj.node_list{i}.ij_deformed = new_ij;
            end
        end
        
        function obj = deform_2(obj)
            % apply deformation from ij and set ij_deformed
            % shear
            
            L = length(obj.node_list);
            for i = 1:L % for all nodes
                % get current node coordinates
                curr_ij = obj.node_list{i}.ij;
                
                % apply deformation mapping
                f_shear = @(ij,c_x,c_y) [1 c_y; c_x 1]*[ij(1); ij(2)];
                new_ij = f_shear(curr_ij,0.5,0);
                
                % set ij_deformed for current node
                obj.node_list{i}.ij_deformed = new_ij;
            end
        end
        
        function obj = deform_3(obj)
            % apply deformation from ij and set ij_deformed
            % scale
            
            L = length(obj.node_list);
            for i = 1:L % for all nodes
                % get current node coordinates
                curr_ij = obj.node_list{i}.ij;
                
                % apply deformation mapping
                f_scale = @(ij,c_x,c_y) [c_y 0; 0 c_x]*[ij(1); ij(2)];
                new_ij = f_scale(curr_ij,2,1);
                
                % set ij_deformed for current node
                obj.node_list{i}.ij_deformed = new_ij;
            end
        end
        
        function obj = deform_4(obj)
            % apply deformation from ij and set ij_deformed
            % wavy_i
            
            L = length(obj.node_list);
            for i = 1:L % for all nodes
                % get current node coordinates
                curr_ij = obj.node_list{i}.ij;
                
                % apply deformation mapping
                new_i = curr_ij(1) + obj.r/2/pi *0.35 * sin(2*pi/obj.r*curr_ij(1));
                new_j = curr_ij(2);
                new_ij = [new_i, new_j];
                
                % set ij_deformed for current node
                obj.node_list{i}.ij_deformed = new_ij;
            end
        end
        
        function target_img = interpolate(obj)
            % barycentric interpolation
            % given deformed sampling "mesh" M
            % find points of target sampling grid in all triangles in M
            % and interpolate its value if inside a triangle
            %
            % ex. target sampling grid: 1920 x 1080
            % return target_img of size [target_rows, target_cols, target_channels] // same as obj.img
            
            L = length(obj.triangle_list);
            
            % get sampling grid
            [i,j] = meshgrid(1:obj.r,1:obj.c);
            i = i(:);
            j = j(:);
            ij_points = [i,j]; % [i1 j1; i2 j2; ...] // [r*c,2]
            
            target_img = zeros(size(obj.img));
            for k = 1:obj.r*obj.c % for each sampled point, find if it is in a triangle
                P = ij_points(k,:); % current point [i,j] // not [x,y]
                
                for i = 1:L % for all triangles of the deformed image
                    curr_triangle = obj.triangle_list{i};
                    P1 = curr_triangle.vertex_1.ij_deformed;
                    P2 = curr_triangle.vertex_2.ij_deformed;
                    P3 = curr_triangle.vertex_3.ij_deformed;
                    
                    % check if P is in the triangle formed by P1 P2 P3
                    result = check_in_triangle(P,P1,P2,P3);
                    
                    if result % if point is in triangle, interpolate and get value
                        % perform interpolation
                        P1_value = curr_triangle.vertex_1_value;
                        P2_value = curr_triangle.vertex_2_value;
                        P3_value = curr_triangle.vertex_3_value;
                        
                        lambda = obj.cartesian_to_barycentric(P1,P2,P3,P);
                        P_value = obj.compute_interpolation(P1_value,P2_value,P3_value,lambda);
                        if abs(1-sum(lambda))>1e-6
                            disp(sum(lambda)) % DEBUG; sum of lambda != 1
                        end
                        break;
                    else
                        % point is not in this triangle; set value to scalar 0
                        P_value = 0;
                        % TODO: return a alpha mask
                    end
                    % note: if point P lies in >1 triangle (b/c of floating point precision issues),
                    % currently use last true result
                    %
                    % TODO: what about holes? (P on boundary of 2 or 3 triangles, but result evaluates to false) Give triangles a boundary thickness
                end
                
                % set P_value into image
                if result
                    target_img(P(1),P(2),:) = P_value(:);
                end
            end
            
            % save target_img to obj
            obj.target_img = target_img;
        end
        
        
    end
    methods(Static)
        % for check functions: i,j is current row,column; r,c is obj.r,obj.c
        function out = check_left(i,j,r,c)
            if rem(j,c) == 1
                out = true;
            else
                out = false;
            end
        end
        function out = check_right(i,j,r,c)
            if rem(j,c) == 0
                out = true;
            else
                out = false;
            end
        end
        function out = check_top(i,j,r,c)
            if rem(i,r) == 1
                out = true;
            else
                out = false;
            end
        end
        function out = check_bottom(i,j,r,c)
            if rem(i,r) == 0
                out = true;
            else
                out = false;
            end
        end
        function out = check_top_left_corner(i,j,r,c)
            if Barycentric.check_top(i,j,r,c) && Barycentric.check_left(i,j,r,c)
                out = true;
            else
                out = false;
            end
        end
        function out = check_top_right_corner(i,j,r,c)
            if Barycentric.check_top(i,j,r,c) && Barycentric.check_right(i,j,r,c)
                out = true;
            else
                out = false;
            end
        end
        function out = check_bottom_left_corner(i,j,r,c)
            if Barycentric.check_bottom(i,j,r,c) && Barycentric.check_left(i,j,r,c)
                out = true;
            else
                out = false;
            end
        end
        function out = check_bottom_right_corner(i,j,r,c)
            if Barycentric.check_bottom(i,j,r,c) && Barycentric.check_right(i,j,r,c)
                out = true;
            else
                out = false;
            end
        end
        
        function node = connect_to_left(node,i,j,r,c)
            
        end
        
        function h = show_sampling_grid(h)
            figure(h);
            xlimits = get(gca,'xlim');
            ylimits = get(gca,'ylim');
            
            [i,j] = meshgrid(-100:100,-100:100);
            hh = surf(j,i,i*0);
            set(hh,'facealpha',0)
            set(hh,'edgealpha',0.5)
            set(gca,'xlim',xlimits)
            set(gca,'ylim',ylimits)
        end
        
        function lambda = cartesian_to_barycentric(P1,P2,P3,P)
            % given vertices of a triangle P1 P2 P3 of the form [i,j] // (1,2)
            % and point P [i,j] inside the triangle,
            % return barycentric coordinates [lambda_1, lambda_2, lambda_3] // (3,1)
            x1 = P1(2); y1 = P1(1);
            x2 = P2(2); y2 = P2(1);
            x3 = P3(2); y3 = P3(1);
            x = P(2); y = P(1);
            
            % A * lambda = [x; y; 1] // solve for lambda
            A = [x1,x2,x3; y1,y2,y3; 1 1 1];
            lambda = A\[x; y; 1]; %inv(A)*[x; y; 1];
        end
        
        function P_value = compute_interpolation(P1_value,P2_value,P3_value,lambda)
            % Px_value: [R G B] value at point Px
            % lambda: barycentric coordinates for point P
            % P_value: [R G B] value at point P, with same size as Px
            
            % https://en.wikipedia.org/wiki/Barycentric_coordinate_system#Interpolation_on_a_triangular_unstructured_grid
            
            P_value = lambda(1)*P1_value + lambda(2)*P2_value + lambda(3)*P3_value;
        end
        
    end
end