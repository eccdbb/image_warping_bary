classdef Node < handle
    properties
        id {mustBeInteger, mustBePositive, mustBeReal}
        RGB % [R,G,B] in [0,1] // RGB value at this node
        ij {mustBeReal} % [i,j] // original coordinates (row,column) at this node
        ij_deformed {mustBeReal} % coordinates after grid deformation
        triangles % cell vector storing Node instances; each element is a triangle
    end
    properties (Constant)
        
    end
    methods
        function obj = Node(id)
            obj.id = id;
            obj.RGB = [NaN,NaN,NaN];
            obj.ij = [NaN,NaN];
            obj.ij_deformed = [NaN,NaN];
            obj.triangles = {};
        end
    end
    methods (Static)
        
    end
end

% ex. set two triangles:
% x1 = Node(1);
% x2 = Node(2);
% x3 = Node(3);
% x4 = Node(4);
% x5 = Node(5);
% x6 = Node(6);
% obj.triangles{1} = {x1, x2, x3}
% obj.triangles{2} = {x3, x5, x6}
%
% to get first triangle vertices:
% for k = 1:3, triangle_vertex = obj.triangles{1}{k}.ij;, end