# Image Warping with Triangle Mesh and Barycentric Coordinates
See test_barycentric.m

- generate a triangle mesh that covers the image
- deform the mesh (affine transformations, random perturbation)
- use barycentric interpolation to calculate the color within the triangle
  - for now, use inefficient triangle intersection test
- apply color to deformed image