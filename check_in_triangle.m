function result = check_in_triangle(P,P1,P2,P3)
% given a point P = [x,y]
% and 3 points  P1 = [x1,y1], P2 = [x2,y2], and P3 = [x3,y3]
% check that P is in the triangle formed by P1 P2 P3
% P, P1, P2, P3 are row vectors

P1 = reshape(P1,1,[]);
P2 = reshape(P2,1,[]);
P3 = reshape(P3,1,[]);
P = reshape(P,1,[]);

Ptri=[P1;P2;P3];
if P(1)<=max(Ptri(:,1))&&P(1)>=min(Ptri(:,1))&&P(2)<=max(Ptri(:,2))&&P(2)>=min(Ptri(:,2)) % AABB pre-check
    
    P12 = P1-P2; P23 = P2-P3; P31 = P3-P1;
    t = sign(det([P31;P23]))*sign(det([P3-P;P23])) >= 0 & ...
        sign(det([P12;P31]))*sign(det([P1-P;P31])) >= 0 & ...
        sign(det([P23;P12]))*sign(det([P2-P;P12])) >= 0 ;
else
    t = false;
end    
result = t;
end
% https://www.mathworks.com/matlabcentral/answers/277984-check-points-inside-triangle-or-on-edge-with-example